
const puppeteer = require('puppeteer');

async function takeContent(url, site) {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    var content = '';
    try {
      await page.goto(url);
      content = await page.content();
    } catch (e) {
      console.log(e);
    }
  
    await browser.close();
    return content;
  }

  module.exports = takeContent();
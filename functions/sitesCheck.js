const checkWords = require('./checkWords');
const cheerio = require('cheerio');
const Site = require('./site.js');
const takeContent = require('./takeContent');


async function sitesCheck(page) {
    var site = Site;
    site.name = page
    site.subPages = [];
    site.subPagesContent = [];
    //console.log(site.name)
    let html = await rp(site.name);
    //console.log(html)
    var $ = cheerio.load(html);
    var href = $("a").each(function() {
      var a = $(this).attr('href');
      site.subPages.push(a);
    });
    //console.log('------------------')
    site.subPages.push(site.name);
    var link;
    var amount = site.subPages.length;
    var stringOne = '';
    var stringTwo = '';
    var howManyTimes = 0;
    for (const item of site.subPages) {
      stringOne += '*';
      howManyTimes++;
      stringTwo = '';
      for (var amounCounter = 0; amounCounter < amount - howManyTimes; amounCounter++) {
        stringTwo += '-';
      }
      console.log('\033[2J');
      console.log('[' + stringOne + stringTwo + ']')
      if (site.visited.includes(item)) continue;
      if (item == undefined || item.includes('.pdf')) continue;
      if (!(item.includes('http://') || item.includes('https://'))) {
        //console.log('ert')
        if (!item.includes('www')) {
          if (!item.includes('.')) {
            if (!(item.includes('(') || item.includes(')') || item.includes(';') || item.includes(',') || item.includes('#'))) {
              link = site.name + item;
            } else {
              continue;
            }
          } else {
            if (item.includes('.html') || item.includes('.php') || item.includes('.ejs')) {
              link = site.name + item;
            } else {
              link = 'www.' + item;
            }
          }
        }
      } else {
        link = item;
      }
  
      //console.log(link);
  
      await takeContent(link).then(val => checkWords(val, page, link))
      site.visited.push(link);
    }
    return 0;
  
  
    //await rp(site.name, async function(err, response, html) {
  
  
  
    //});
  }

  module.exports = sitesCheck();
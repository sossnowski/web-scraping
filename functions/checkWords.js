const cheerio = require('cheerio');
const connection = require('./db');


async function checkWords(content, mainName, subName) {

    //console.log(content.length)
    $ = cheerio.load(content);
    //console.log($)
    var text = $('body').text();
    var pureText = text.replace(/\s+/g, '');
    //console.log(text)
    //read file wheather there are diffrences
    connection.query("INSERT INTO contents SET ?", {
      mainName: mainName,
      subName: subName,
      content: pureText
    }, function(err, result) {
      if (err) {
        console.log(err);
      }
    });
    for (var i = 0; i < text.length; i++) {
      for (var z = 0; z < keyWords.length; z++) {
        var isCorrectWord = '';
  
        for (var j = i; j < i + keyWords[z].length; j++) {
          isCorrectWord += text[j];
        }
        //console.log(isCorrectWord);
        if (keyWords[z] == isCorrectWord) {
          find = true;
          if (!found.includes(isCorrectWord)) {
            found.push(isCorrectWord);
            var allPhrase = '';
            var counter = i;
            while (text[counter] != '\n') {
              counter--;
            }
            counter++;
            while (text[counter] != '\n') {
              allPhrase += text[counter];
              counter++;
            }
            //console.log("---" + allPhrase);
            //save to db
            connection.query("INSERT INTO keyWords SET ?", {
              word: isCorrectWord,
              domain: subName,
              text: allPhrase
            }, function(err, result) {
              if (err) {
                console.log(err);
              }
            });
          }
        }
      }
    }
    return true;
  }

  module.exports = checkWords();